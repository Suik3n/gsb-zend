<?php

require_once 'LigneFraisForfait.php';
require_once 'Forfait.php';

class StatsForfaitsController extends Zend_Controller_Action {
    
    public function init() {
        
    }
    
    public function indexAction() {
        $this->view->title = "Statistiques sur les Forfaits";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        
        $query = "SELECT idvisiteur, mois, idFraisForfait, montant * quantite as total
            FROM LigneFraisForfait lff, FraisForfait ff
            WHERE lff.idFraisForfait = ff.id";
        
        $lesStats = $db->fetchAll($query);
        
        $this->view->lesStats = $lesStats;
    }
}

?>