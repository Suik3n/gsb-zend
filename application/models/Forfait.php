<?php

class Forfait extends Zend_Db_Table_Abstract {

    protected $_name = 'FraisForfait';
    protected $_primary = 'id';
    protected $_rowClass = 'ForfaitRow';

    public function getForfait($id) {
        $row = $this->fetchRow("id='" . $id . "'");
        if (!$row) {
            throw new Exception("...ne trouve pas le frais forfait");
        }
        return $row->toArray();
    }

    public function modifierForfait($id, $libelle, $montant) {
        $data = array(
            'id' => $id,
            'libelle' => $libelle,
            'montant' => $montant
        );
        $this->update($data, "id='" . $id . "'");
    }

}

?>