<?php

class LigneFraisForfait extends Zend_Db_Table_Abstract {

    protected $_name = 'LigneFraisForfait';
    protected $_primary = array('idVisiteur', 'mois', 'idFraisForfait');
    
    protected $_referenceMap = array(
        'refVisiteur' => array(
            'columns' => array('idVisiteur'),
            'refTableClass' => 'Visiteur',
            'refColumns' => array('id')
        ),
        'refFraisForfait' => array(
            'columns' => array('idFraisForfait'),
            'refTableClass' => 'FraisForfait',
            'refColumns' => array('id')
        )
    );
}

?>
