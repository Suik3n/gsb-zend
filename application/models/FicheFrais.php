<?php

class FicheFrais extends Zend_Db_Table_Abstract {

    protected $_name = 'FicheFrais';
    protected $_primary = array('idVisiteur', 'mois');
    protected $_referenceMap = array(
        'refVisiteur' => array(
            'columns' => 'idVisiteur',
            'refTableClass' => 'Visiteur',
            'refColumns' => 'id'
        ),
        'refEtat' => array(
            'columns' => 'idEtat',
            'refTableClass' => 'Etat',
            'refColumns' => 'id'
        )
    );

}

?>
