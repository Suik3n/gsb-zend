<?php

class Visiteur extends Zend_Db_Table_Abstract {

    protected $_name = 'Visiteur';
    protected $_primary = 'id';

    public function getVisiteur($id) {
        $row = $this->fetchRow("id='" . $id . "'");
        if (!$row) {
            throw new Exception("...ne trouve pas le visiteur identifié par $id");
        }
        return $row->toArray();
    }

    public function ajouterVisiteur($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $dateEmbauche) {
        $data = array(
            'id' => $id,
            'nom' => $nom,
            'prenom' => $prenom,
            'login' => $login,
            'mdp' => $mdp,
            'adresse' => $adresse,
            'cp' => $cp,
            'ville' => $ville,
            'dateEmbauche' => $dateEmbauche
        );
        $this->insert($data);
    }

    public function modifierVisiteur($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $dateEmbauche) {
        $data = array(
            'id' => $id,
            'nom' => $nom,
            'prenom' => $prenom,
            'login' => $login,
            'mdp' => $mdp,
            'adresse' => $adresse,
            'cp' => $cp,
            'ville' => $ville,
            'dateEmbauche' => $dateEmbauche
        );
        $this->update($data, "id='" . $id . "'");
    }

    public function supprimerVisiteur($id) {
        $this->delete("id='" . $id . "'");
    }

}

?>
