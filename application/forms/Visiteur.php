<?php

class Form_Visiteur extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->setName('visiteur');

        $id = new Zend_Form_Element_Text('id');
        $id->setLabel('Id')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $nom = new Zend_Form_Element_Text('nom');
        $nom->setLabel('Nom')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $prenom = new Zend_Form_Element_Text('prenom');
        $prenom->setLabel('Prénom')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $login = new Zend_Form_Element_Text('login');
        $login->setLabel('Login')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $mdp = new Zend_Form_Element_Text('mdp');
        $mdp->setLabel('Mot de passe')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $adresse = new Zend_Form_Element_Text('adresse');
        $adresse->setLabel('Adresse')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $cp = new Zend_Form_Element_Text('cp');
        $cp->setLabel('Code postal')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $ville = new Zend_Form_Element_Text('ville');
        $ville->setLabel('Ville')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $dateEmbauche = new Zend_Form_Element_Text('dateEmbauche');
        $dateEmbauche->setLabel("Date d'embauche")
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->setDescription("format AAAA-MM-JJ");

        $envoyer = new Zend_Form_Element_Submit('envoyer');
        $envoyer->setAttrib('id', 'boutonenvoyer');

        $this->addElements(array($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $dateEmbauche, $envoyer));
    }

}

?>
