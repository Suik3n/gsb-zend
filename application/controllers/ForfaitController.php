<?php require_once 'Forfait.php';

class ForfaitController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $this->view->title = "Gérer les forfaits";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        $forfait = new Forfait($db);
        $this->view->lesForfaits = $forfait->fetchAll();
    }

    public function modifierAction() {
        $form = new Form_Forfait();
        $form->envoyer->setLabel('Modifier');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $libelle = $form->getValue('libelle');
                $montant = $form->getValue('montant');
                $lesFraisForfaits = new Visiteur();
                $lesFraisForfaits->modifierFraisForfait($id, $libelle, $montant);
                $this->_redirect('/visiteur');
            } else {
                $form->populate($formData);
            }
        } else {
            $idForfait = $this->_getParam('id', 0);
            $lesForfaits = new FraisForfait();
            $form->populate($lesForfaits->getVisiteur($idForfait));
        }
    }

    public function supprimerAction() {
        if ($this->getRequest()->isPost()) {
            $supprimer = $this->getRequest()->getPost('supprimer');
            if ($supprimer == 'Oui') {
                $id = $this->getRequest()->getPost('id');
                $lesVisiteurs = new Visiteur();
                $lesVisiteurs->supprimerVisiteur($id);
            }

            $this->_redirect('/visiteur');
        } else {
            $id = $this->_getParam('id', 0);
            $lesVisiteurs = new Forfait();
            $this->view->visiteur = $lesVisiteurs->getForfait($id);
        }
    }

    public function preDispatch() {
        
    }

    public function postDispatch() {
        
    }

}
