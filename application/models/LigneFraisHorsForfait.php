<?php

class LigneFraisHorsForfait extends Zend_Db_Table_Abstract {

    protected $_name = 'LigneFraisHorsForfait';
    protected $_primary = 'id';
    protected $_referenceMap = array(
        'refVisiteur' => array(
            'columns' => 'idVisiteur',
            'refTableClass' => 'Visiteur',
            'refColumns' => 'id'
        )
    );

}

?>
