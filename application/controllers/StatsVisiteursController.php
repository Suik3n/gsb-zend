<?php

require_once 'FicheFrais.php';

class StatsVisiteursController extends Zend_Controller_Action {
    
    public function init() {
        
    }
    
    public function indexAction() {
        $this->view->title = "Statistiques sur les Visiteurs";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        
        $query = "select * from FicheFrais group by idVisiteur, mois order by idVisiteur";
        
        $lesStats = $db->fetchAll($query);
        
        $this->view->lesStats = $lesStats;
    }
}

?>