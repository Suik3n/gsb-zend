<?php

require_once 'Visiteur.php';

class VisiteurController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $this->view->title = "Gérer les visiteurs";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $registry = Zend_Registry::getInstance();
        $db = $registry->get('db');
        $vis = new Visiteur($db);
        $this->view->lesVisiteurs = $vis->fetchAll();
    }
    
    public function ajouterAction() {
        $form = new Form_Visiteur();
        $form->envoyer->setLabel('Ajouter');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $nom = $form->getValue('nom');
                $prenom = $form->getValue('prenom');
                $login = $form->getValue('login');
                $mdp = $form->getValue('mdp');
                $adresse = $form->getValue('adresse');
                $cp = $form->getValue('cp');
                $ville = $form->getValue('ville');
                $dateEmbauche = $form->getValue('dateEmbauche');
                $lesVisiteurs = new Visiteur();
                $lesVisiteurs->ajouterVisiteur($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $dateEmbauche);
                $this->_redirect('/visiteur');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function modifierAction() {
        $form = new Form_Visiteur();
        $form->envoyer->setLabel('Modifier');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $nom = $form->getValue('nom');
                $prenom = $form->getValue('prenom');
                $login = $form->getValue('login');
                $mdp = $form->getValue('mdp');
                $adresse = $form->getValue('adresse');
                $cp = $form->getValue('cp');
                $ville = $form->getValue('ville');
                $dateEmbauche = $form->getValue('dateEmbauche');
                $lesVisiteurs = new Visiteur();
                $lesVisiteurs->modifierVisiteur($id, $nom, $prenom, $login, $mdp, $adresse, $cp, $ville, $dateEmbauche);
                $this->_redirect('/visiteur');
            } else {
                $form->populate($formData);
            }
        } else {
            $idvis = $this->_getParam('idvis', 0);
            $lesVisiteurs = new Visiteur();
            $form->populate($lesVisiteurs->getVisiteur($idvis));
        }
    }

    public function supprimerAction() {
        if ($this->getRequest()->isPost()) {
            $supprimer = $this->getRequest()->getPost('supprimer');
            if ($supprimer == 'Oui') {
                $id = $this->getRequest()->getPost('id');
                $lesVisiteurs = new Visiteur();
                $lesVisiteurs->supprimerVisiteur($id);
            }

            $this->_redirect('/visiteur');
        } else {
            $id = $this->_getParam('idvis', 0);
            $lesVisiteurs = new Visiteur();
            $this->view->visiteur = $lesVisiteurs->getVisiteur($id);
        }
    }

    public function preDispatch() {
        
    }

    public function postDispatch() {
        
    }

}
