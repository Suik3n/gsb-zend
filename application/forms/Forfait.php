<?php

class Form_Forfait extends Zend_Form {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->setName('forfait');

        $id = new Zend_Form_Element_Text('id');
        $id->setLabel('Id')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $libelle = new Zend_Form_Element_Text('libelle');
        $libelle->setLabel('Libelle')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $montant = new Zend_Form_Element_Text('montant');
        $montant->setLabel('Montant')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty');

        $envoyer = new Zend_Form_Element_Submit('envoyer');
        $envoyer->setAttrib('id', 'boutonenvoyer');

        $this->addElements(array($id, $libelle, $montant, $envoyer));
    }

}

?>
